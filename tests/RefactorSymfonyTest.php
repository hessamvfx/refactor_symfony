<?php


namespace App\Tests;


use App\Services\CommissionCalculatorService;
use App\Services\Provider\BinProvider;
use App\Services\Provider\BinProviderInterFace;
use App\Services\Provider\CurrencyRateProvider;
use App\Services\Provider\CurrencyRateProviderInterFace;
use App\Tests\DataProvider\DataProviders;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class RefactorSymfonyTest  extends TestCase

{

    use DataProviders;

    /**
     * @dataProvider isEuDataProvider
     * @throws \ReflectionException
     */
    public function testGetCommissionRate($code, $expected)
    {
        $binProvider = $this->createMock(BinProviderInterFace::class);
        $rateProvider = $this->createMock(CurrencyRateProviderInterFace::class);
        $inputRow = ['bin' => 45717360, 'amount' => 100.00, 'currency' => 'EUR'];
        $calculator = new CommissionCalculatorService($binProvider, $rateProvider, $inputRow,'EUR');

        $reflector = new ReflectionClass(CommissionCalculatorService::class);
        $method = $reflector->getMethod('getCommissionRate');
        $method->setAccessible(true);
        $result = $method->invoke($calculator, $code);
        $this->assertEquals($expected, $result);
    }
    public function isEuDataProvider(): array
    {
        return [
            [false, 0.02],
            [true, 0.01],
            [true, 0.01],
            [false, 0.02]
        ];
    }

    /**
     * @dataProvider JsonDataProvider
     */
    public function testCommissionCalculation($binData, $row, $rate, $expected)
    {

        $binProvider = $this->createMock(BinProvider::class);
        $binProvider->method('callCurl')
            ->willReturn($binData);

        $rateProvider = $this->createMock(CurrencyRateProvider::class);

        $rateProvider->method('response')
            ->willReturn($rate);
        $calculator = new CommissionCalculatorService($binProvider, $rateProvider, $row,'EUR');


        $this->assertEquals($expected, $calculator->calculate());
    }

    /**
     * @dataProvider JsonDataProvider
     */
    public function testCommissionCalculationWithNoRate($binData, $row, $rate, $expected)
    {

        $binProvider = $this->createMock(BinProvider::class);
        $binProvider->method('callCurl')
            ->willReturn($binData);

        $rateProvider = $this->createMock(CurrencyRateProvider::class);

        $rateProvider->method('response')
            ->willReturn($rate);
        $calculator = new CommissionCalculatorService($binProvider, $rateProvider, $row,'EUR');


        $this->assertEquals($expected, $calculator->calculate());
    }

}