<?php

namespace App\Tests\DataProvider;

use App\Tests\DataProvider\Provider\Json;

trait DataProviders
{

    /**
     * @throws \ReflectionException
     * @throws Provider\InvalidSetException
     */
    public function jsonDataProvider($testMethodName)
    {
        return (new Json($this))->getFixtures($testMethodName);
    }

}
