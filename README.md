# refactor_symfony


## Install

composer install

## Run

php bin/console app:calculate-commission

## Run test

php ./vendor/bin/phpunit

## Notice
I used https://api.exchangerate.host/latest  because https://api.exchangeratesapi.io/latest  was not free
