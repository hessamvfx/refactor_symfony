<?php

namespace App\Command;

use App\Services\CommissionCalculatorService;
use App\Services\Provider\BinProvider;
use App\Services\Provider\CurrencyRateProvider;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A console command that calculate commission and print in terminal.
 *
 * To use this command, open a terminal window, enter into your project
 * directory and execute the following:
 *
 *     $ php bin/console app:calculate-commission
 *
 * To output detailed information, increase the command verbosity:
 *
 *     $ php bin/console app:calculate-commission -vv
 *
 * See https://symfony.com/doc/current/console.html
 *
 * We use the default services.yaml configuration, so command classes are registered as services.
 * See https://symfony.com/doc/current/console/commands_as_services.html
 *
 * @author hessam zaheri <hessamvfx@gmail.com>
 */
#[AsCommand(
    name: 'app:calculate-commission',
    description: 'calculate commission and print in terminal'
)]
class CalculateCommissionCommand extends Command
{

    public function __construct(private string $binUrl, private string $rateUrl, private string $baseCurrency)
    {
        parent::__construct();
    }


    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->addArgument('input-file', null, InputOption::VALUE_REQUIRED, 'input.txt') ;
    }


    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $data=$this->getFileData($input->getArgument('input-file'));
        foreach ($data as $item) {
            $row = json_decode($item, true);
            $binProvider = new BinProvider($this->binUrl, $row['bin']);
            $rateProvider = new CurrencyRateProvider($this->rateUrl, $row['currency']);
            $calculator = new CommissionCalculatorService($binProvider, $rateProvider, $row, $this->baseCurrency);
            echo $calculator->calculate();
            print "\n";
        }
        return  1;
    }


    private function getFileData(string $filename):array
    {
        $contents = @file_get_contents($filename);
        if ($contents === false) {
            throw new Exception('File not exit');
        }
        return explode("\n", $contents);
    }
}
