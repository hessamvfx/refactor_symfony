<?php

namespace App\Services\Provider;

class CurrencyRateProvider extends BaseProvider implements CurrencyRateProviderInterFace
{

    /**
     * @param string $url
     * @param string $currency
     */
    public function __construct(protected string $url, private string $currency)
    {
        $this->urlAddress=$this->url;
    }

    public function response(array|null $output):string
    {

        if (!empty($output['rates'][$this->currency])) {
            return $output['rates'][$this->currency];
        }
        throw new \Exception('invalid answer');
    }
}
