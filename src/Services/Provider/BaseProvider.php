<?php

namespace App\Services\Provider;

use Exception;

class BaseProvider
{
    protected string $urlAddress;

    /**
     * @throws Exception
     */
    public function callCurl(): array|null
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->urlAddress);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $error = curl_error($ch);
        if ($error) {
            throw new \Exception($error);
        }
        $result = json_decode($data, true, JSON_PRETTY_PRINT);
        curl_close($ch);
        return $result;
    }

}
