<?php

namespace App\Services\Provider;

interface BinProviderInterFace
{
    public function getCountryCode(array $data):string|null;
}