<?php

namespace App\Services\Provider;

class BinProvider extends BaseProvider implements BinProviderInterFace
{

    public function __construct(private string $url, private string $bin)
    {
        $this->urlAddress=$this->url.$this->bin;
    }

    public function getCountryCode(array $data):string|null
    {
        if ($data['country'] && isset($data['country']['alpha2']) !== null) {
            return $data['country']['alpha2'];
        }
        return null;
    }
}
