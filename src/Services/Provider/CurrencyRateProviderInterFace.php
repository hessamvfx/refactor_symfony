<?php

namespace App\Services\Provider;

interface CurrencyRateProviderInterFace
{
    public function response(array|null $output);
}