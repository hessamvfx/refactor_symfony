<?php

namespace App\Services;

use App\Services\Provider\BinProviderInterFace;
use App\Services\Provider\CurrencyRateProviderInterFace;

class CommissionCalculatorService
{
    const EU_CODE = ['AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT',
        'LT', 'LU', 'LV', 'MT', 'NL', 'PO', 'PT', 'RO', 'SE', 'SI', 'SK'];

    /**
     * @var float
     */
    private float $amount;
    /**
     * @var string
     */
    private string $currency;

    public function __construct(
        private BinProviderInterFace $binProvider,
        private CurrencyRateProviderInterFace $currencyRateProvider,
        private array $row,
        private string $baseCurrency
    ) {
        $this->bin = $row["bin"];
        $this->amount = $row["amount"];
        $this->currency = $row["currency"];
    }

    /**
     * @throws \Exception
     */
    public function calculate(): float
    {
        $binData=$this->binProvider->callCurl();
        $currencyRateResult=$this->currencyRateProvider->callCurl();
        $currencyRateData=$this->currencyRateProvider->response($currencyRateResult);
        $amount=$this->calculateFixAmount($currencyRateData);
        $countryCode=$this->binProvider->getCountryCode($binData);
        $isEu = $this->isEu($countryCode);
        $commissionRate = $this->getCommissionRate($isEu);
        return $this->applyCommission($amount, $commissionRate);
    }


    private function calculateFixAmount(int|float|string $rate)
    {
        if ($this->currency === $this->baseCurrency || $rate == 0) {
            return $this->amount;
        } elseif ($this->currency !=$this->baseCurrency|| $rate > 0) {
            return $this->amount / $rate;
        }else{
            return $this->amount;
        }

    }


    private function isEu($countryCode):bool
    {
        return in_array($countryCode, self::EU_CODE);
    }

    private function getCommissionRate($isEu):float
    {
        return $isEu ? 0.01 : 0.02;
    }
    public function applyCommission($amount, $commissionRate):float
    {
        return round($amount * $commissionRate, 2);
    }
}
